$(function () {
  var APPLICATION_ID = "5DDA19C4-CD43-5666-FF6B-6FF9D40E3D00",
      SECRET_KEY = "93B31DB4-256C-4D7D-FF56-2E7BE6583100",
      VERSION = "v1";
      
   Backendless.initApp(APPLICATION_ID, SECRET_KEY, VERSION);
   
   Backendless.UserService.logout();
   
   if(Backendless.UserService.isValidLogin()){
       userLoggedIn(Backendless.LocalCache.get("current-user-id"));
   } else {
       var loginScript = $("#login-template").html();
       var loginTemplate = Handlebars.compile(loginScript);
       $('.main-container').html(loginTemplate);
   }
   
   $(document).on('submit', '.form-signin', function(event){
       event.preventDefault();
       
       var data = $(this).serializeArray(),
           email = data[0].value,
           password = data[1].value;
           
       Backendless.UserService.login(email, password, true, new Backendless.Async(userLoggedIn, gotError));
   });
   
   $(document).on('click','.add-blog', function(){
           var addBlogScript = $("#add-blog-template").html();
           var addBlogTemplate = Handlebars.compile(addBlogScript);
           
           $('.main-container').html(addBlogTemplate);
   });
   $(document).on('submit', '.form-add-blog', function(event){
       event.preventDefault();
       
       var data = $(this).serializeArray(),
           title = data[0].value,
           content = data [1].value;
               if ( content === "" || title === ""){
                   Materialize.toast('Empty?! Fill er up!', 4000);
               }
               else{

                   var dataStore = Backendless.Persistence.of(Posts);

                   var postObject = new Posts({
                       title: "title",
                       content: "content",
                       authorEmail: Backendless.UserService.getCurrentUser().email
                   });

                   Materialize.toast('Posted', 4000);
                   dataStore.save(postObject);
       
                   this.title.value = "";
                   this.content.value  = "";
               }           
       
   });
   
   $(document).on('click', '.logout', function (){
       Backendless.UserService.logout(new Backendless.Async(userLoggedOut, gotError)); 
      
       var loginScript = $("#login-template").html();
       var loginTemplate = Handlebars.compile(loginScript);
   
       $('.main-container').html(loginTemplate);
   });

});

function Posts(args){
   args = args || {};
   this.title = args.title || "";
   this.content = args.content || "";
   this.authorEmail = args.authorEmail || "";
   
}

function userLoggedIn(user) {
   console.log("user successfully logged in");
   }
function gotError(error) {
    console.log("Error message -" + error.message);
    console.log("Error code -" + error.code);
    Materialize.toast('Uh Oh! Incorrect Loggin', 4000);
}